@extends("layouts.index")
@section('title','sign up')
@section('content')
<div class="container sign-up">
    <h2>Sign Up</h2>
    <form method="post" action="{{URL::to('/')}}/signup">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password">
        </div>
        <div class="form-group">
            <label for="password">Confirm Password</label>
            <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password">
        </div>
        <button type="submit" class="btn btn-default">Sign Up</button>
    </form>
</div>
@endsection