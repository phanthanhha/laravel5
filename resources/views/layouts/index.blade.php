<html>
    <head>
        <title>@yield('title')</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/style.css">
        <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <a href="#"><img  class="logo" src="{{URL::to('/')}}/images/blog.png"></a>
            </div>
        </nav>
        @yield('content')
        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
            </div>
        </nav>
    </body>
</html>